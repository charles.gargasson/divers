#############
OpenFire Exec
#############

| Execute System Command (pentest purpose)
|
| Example of plugin:
| https://github.com/igniterealtime/openfire-exampleplugin
| 
| Plugin creation documentation
| https://download.igniterealtime.org/openfire/docs/latest/documentation/plugin-dev-guide.html
|
| List of released plugins
| https://www.igniterealtime.org/projects/openfire/plugins.jsp
|
|

*****
Build
*****

.. code-block:: bash

    apt install maven
    cd openfirejspexec/
    bash build.sh

|

*******
Screens
*******

| Usage

.. image:: screen1.png
    :width: 400

|