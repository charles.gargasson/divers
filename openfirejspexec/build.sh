#!/bin/bash
mvn compile
mvn package
jar -uf target/execsystemcommand.jar plugin.xml
jar -uf target/execsystemcommand.jar readme.html
jar -uf target/execsystemcommand.jar changelog.html
cp target/execsystemcommand.jar /tmp/execsystemcommand.jar
ls -ltrha /tmp/execsystemcommand.jar

