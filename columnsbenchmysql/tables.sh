cd $(dirname $0)
set -x

python3 maketables.py > tables.sql
ls -ltrh tables.sql

docker stop mysql
docker run --rm -d \
  --name mysql \
  -e MYSQL_DATABASE=database \
  -e MYSQL_ROOT_PASSWORD=password \
  -v $(pwd)/tables.sql:/tables.sql:ro \
  --network=none \
  --user 1500:1500 \
  --init mysql:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci 

docker ps -a

sleep 20

docker exec -it mysql bash -c "cat /tables.sql|MYSQL_PWD=password mysql -uroot database"

echo "READY !!"
