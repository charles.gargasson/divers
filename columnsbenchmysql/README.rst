#################
Benchmark Columns
#################

| Test if number of columns impact performances of mysql
| Feel free to adjust python settings
|
| Require : python3, docker

BUILD & RUN
###########

.. code-block:: bash

  bash tables.sh

BENCHMARK RESULTS
#################

| mytable0 : 10000 rows 1 col

.. code-block:: bash

  Q='SELECT id from mytable0 where bla0 like "%chat%"'
  docker exec -e MYSQL_PWD=password -it mysql mysql -D database -uroot -e "SET profiling = 1;$Q;SHOW PROFILES;"
  #+----------+------------+--------------------------------------------------+
  #| Query_ID | Duration   | Query                                            |
  #|        1 | 0.00416525 | SELECT id from mytable0 where bla0 like "%chat%" |
  #+----------+------------+--------------------------------------------------+

| mytable1: 10000 rows 100 cols

.. code-block:: bash

  Q='SELECT id from mytable1 where bla0 like "%chat%"'
  docker exec -e MYSQL_PWD=password -it mysql mysql -D database -uroot -e "SET profiling = 1;$Q;SHOW PROFILES;"
  #+----------+------------+--------------------------------------------------+
  #| Query_ID | Duration   | Query                                            |
  #|        1 | 0.00787350 | SELECT id from mytable1 where bla0 like "%chat%" |
  #+----------+------------+--------------------------------------------------+

| mytable2: 10000 rows, 50 cols x2 size except "bla0"

.. code-block:: bash

  Q='SELECT id from mytable2 where bla0 like "%chat%"'
  docker exec -e MYSQL_PWD=password -it mysql mysql -D database -uroot -e "SET profiling = 1;$Q;SHOW PROFILES;"
  #+----------+------------+--------------------------------------------------+
  #| Query_ID | Duration   | Query                                            |
  #|        1 | 0.00668175 | SELECT id from mytable2 where bla0 like "%chat%" |
  #+----------+------------+--------------------------------------------------+

| 2 cols select --- mytable3(10000rows, 2cols)

.. code-block:: bash

  Q='SELECT id FROM mytable3 WHERE bla0="%chat%" OR bla1="%chat%"'
  docker exec -e MYSQL_PWD=password -it mysql mysql -D database -uroot -e "SET profiling = 1;$Q;SHOW PROFILES;"
  #+----------+------------+--------------------------------------------------------------+
  #| Query_ID | Duration   | Query                                                        |
  #|        1 | 0.00281650 | SELECT id FROM mytable3 WHERE bla0="%chat%" OR bla1="%chat%" |
  #+----------+------------+--------------------------------------------------------------+

| 2 cols select --- mytable0(10000rows, 1col) + mytable0bis(10000rows, 1col) with JOIN

.. code-block:: bash

  Q='SELECT mytable0.id FROM mytable0 JOIN mytable0bis ON mytable0.id=mytable0bis.id WHERE mytable0.bla0="%chat%" OR mytable0bis.bla0="%chat%"'
  docker exec -e MYSQL_PWD=password -it mysql mysql -D database -uroot -e "SET profiling = 1;$Q;SHOW PROFILES;"
  #+----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------+
  #| Query_ID | Duration   | Query                                                                                                                                     |
  #+----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------+
  #|        1 | 0.00828850 | SELECT mytable0.id FROM mytable0 JOIN mytable0bis ON mytable0.id=mytable0bis.id WHERE mytable0.bla0="%chat%" OR mytable0bis.bla0="%chat%" |
  #+----------+------------+-------------------------------------------------------------------------------------------------------------------------------------------+

CLEANUP
#######
  
.. code-block:: bash

  docker stop mysql
