#!/usr/bin/env python3

import random
import string

ROWS_MAX=10000
COLS_MAX=100
VARCHAR=30

def randl(l=VARCHAR):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(l))

# TABLE 0
print(f"CREATE TABLE mytable0(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, bla0 VARCHAR({VARCHAR}));")
for x in range(ROWS_MAX):
    print(f"INSERT INTO mytable0 (bla0) VALUES ('{randl()}');")

# TABLE 1
COLS_TABLE=','.join([ f"bla{x} VARCHAR({VARCHAR})" for x in range(COLS_MAX)])
print(f"CREATE TABLE mytable1(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, {COLS_TABLE});")
COLS_INSERT=','.join([ f"bla{x}" for x in range(COLS_MAX)])
for x in range(ROWS_MAX):
    vals = ','.join([f"'{randl()}'" for x in range(COLS_MAX)])
    print(f"INSERT INTO mytable1 ({COLS_INSERT}) VALUES ({vals});")

# TABLE 2
COLS_TABLE=f"bla0 VARCHAR({VARCHAR}),"+','.join([ f"bla{x} VARCHAR({VARCHAR*2})" for x in range(1,int(COLS_MAX/2))])
print(f"CREATE TABLE mytable2(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, {COLS_TABLE});")
COLS_INSERT=','.join([ f"bla{x}" for x in range(int(COLS_MAX/2))])
for x in range(ROWS_MAX):
    vals = f"'{randl()}',"+','.join([f"'{randl(VARCHAR*2)}'" for x in range(1,int(COLS_MAX/2))])
    print(f"INSERT INTO mytable2 ({COLS_INSERT}) VALUES ({vals});")

# TABLE 3
print(f"CREATE TABLE mytable3(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, bla0 VARCHAR({VARCHAR}), bla1 VARCHAR({VARCHAR}));")
for x in range(ROWS_MAX):
    print(f"INSERT INTO mytable3 (bla0,bla1) VALUES ('{randl()}','{randl()}');")

# TABLE 0Bis
print(f"CREATE TABLE mytable0bis(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, bla0 VARCHAR({VARCHAR}));")
for x in range(ROWS_MAX):
    print(f"INSERT INTO mytable0bis (bla0) VALUES ('{randl()}');")