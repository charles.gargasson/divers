import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import json
import syslog 
import os 

syslog.openlog(ident="pyfilesmonitor",logoption=syslog.LOG_PID, facility=syslog.LOG_LOCAL0)

class Watcher:
    def __init__(self):
        self.observer = Observer()
        self.DIRS = [ "/etc/","/tmp/","/var/www/","/boot","/mnt","/root","/www" ]
        self.DIRS.extend(os.environ['PATH'].split(':'))
        self.DIRS = [ x for x in list(set(self.DIRS)) if os.path.exists(x) ]
        
    def run(self):
        event_handler = Handler()
        _ = [ self.observer.schedule(event_handler, x, recursive=True) for x in self.DIRS ]
        self.observer.start()
        print("[INFO] Monitoring ...")
        try: 
            while True: 
                time.sleep(5)
        except :
            self.observer.stop()

        print("\n[INFO] Ending after exception ...")
        self.observer.join()
        print("[INFO] Bye")

class Handler(FileSystemEventHandler):
    def on_any_event(self,event):
        if event.is_directory: return None
        self.output(event.src_path,event.event_type)

    def output(self,path,action):
        r = {"file": path, "action": action}
        r = f"{json.dumps(r)}"
        syslog.syslog(r)
        print(r)

if __name__ == '__main__':
    w = Watcher()
    w.run()
