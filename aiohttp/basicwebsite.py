from aiohttp import web
import asyncio

import logging
logging.basicConfig(level=logging.DEBUG)

async def index(req):
  msg = req.match_info.get('msg')
  return web.Response(text=f"Hey! {msg}, what's up")

def get_app():
  app = web.Application()

  # Defines Routes 
  g = web.get # GET Method
  p = web.post # POST Method
  s = web.static # Static files

  app.add_routes([
    g('/{msg}', index),
  ])
  
  return app

async def Main():
    app = get_app()
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, "127.0.0.1", 8080)
    await site.start()

    while True:
      await asyncio.sleep(3600)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(Main())
