#!/usr/bin/env python3
# USAGE : python3 script.py "URL" TIME WORKERS WORKERCONCURRENCY
# EX : python3 script.py "http://127.0.0.1" 10 8 2

import aiohttp
import asyncio
import uvloop
import sys
from concurrent.futures import ProcessPoolExecutor, as_completed

class HttpClient:
  def __init__(self):
    self.url = sys.argv[1]
    self.time = int(sys.argv[2])
    self.workers = int(sys.argv[3])
    self.workerconcurrency = int(sys.argv[4])

    self.cpt = 0
    self.total = 0
    self.exit = 0

  async def Fetch(self):
    async with self.session.get(self.url) as resp:
      if resp.status != 200:
        print(f"Return:{resp.status}")
      else:
        self.cpt+=1

  async def ConcurrentFetch(self):
    tasks = set()
    async with aiohttp.ClientSession() as self.session:
      while not self.exit:
        if len(tasks) >= self.workerconcurrency:
          _done, tasks = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
        tasks.add(asyncio.create_task(self.Fetch()))
      await asyncio.wait(tasks)

  async def LivePerfs(self):
    while not self.exit:
      await asyncio.sleep(1)
      self.time += -1
      if self.time == 0:
        self.exit = 1

      print(f"Instance {self.instance+1} => {self.cpt} r/s ({self.time} seconds left)")
      self.total += self.cpt
      self.cpt = 0
    return self.total

  async def RunTasks(self):
    tasks = [
      self.ConcurrentFetch(),
      self.LivePerfs()
    ]
    result = await asyncio.gather(*tasks)
    return result[1]
    
  def NewProcess(self, instance):
    self.instance = instance
    uvloop.install()
    return asyncio.run(self.RunTasks())

  def Start(self):
    with ProcessPoolExecutor(max_workers=self.workers) as executor:
      futures = [executor.submit(self.NewProcess, x) for x in range(self.workers)]
      results = [future.result() for future in as_completed(futures)]
      average = int(sum(results)/self.time)
      print(f"Average {average} requests / second")

if __name__ == '__main__':
  HttpClient().Start()

