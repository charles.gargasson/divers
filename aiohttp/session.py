import aiohttp
import asyncio
import hashlib
import re

url = 'http://challenge.com:32599/'

def Parse(html):
  search = re.search("<h3 align='center'>(.*)</h3", html, re.IGNORECASE)
  if search:
    extracted = search.group(1)
    reponse = hashlib.md5(extracted.encode()).hexdigest()
  return {"hash": reponse}

async def Main():
  async with aiohttp.ClientSession(cookie_jar=aiohttp.CookieJar(unsafe=True)) as session:
    async with session.get(url) as resp:
      print(resp.status)
      html = await resp.text()
      print(html)
      data = Parse(html)

    async with session.post(url, data=data) as resp:
      print(resp.status)
      print(await resp.text())

asyncio.run(Main())
