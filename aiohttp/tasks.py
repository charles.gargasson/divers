#!/usr/bin/env python3
import aiohttp
import asyncio

url = 'http://127.0.0.1:80/'
cpt=0

async def Fetch(session):
  async with session.get(url) as resp:
    if resp.status != 200:
      print(f"Return:{resp.status}")
    else:
      global cpt
      cpt+=1

async def ManageTasks():
  tasks = set()
  async with aiohttp.ClientSession() as session:
    while True:
      if len(tasks) >= 10:
        _done, tasks = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
      tasks.add(asyncio.create_task(Fetch(session)))
    await asyncio.wait(tasks)

async def Perfs():
  cpt2=0
  while True:
    await asyncio.sleep(1)
    print(cpt-cpt2)
    cpt2=cpt

async def Main():
  tasks = [
    ManageTasks(),
    Perfs()
  ]
  await asyncio.gather(*tasks)

if __name__ == '__main__':
  asyncio.run(Main())