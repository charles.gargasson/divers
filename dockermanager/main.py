import docker
import time
import logging
logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
import sys
client = docker.from_env()
client.login(username='vtwymuoff', password='nzquinjpxwmdrerp_1A', email='charlesgargasson@gmail.com', registry='https://reg.offensive.run/v2/')

data = {
    "manda-web-offrun" : {
       "ports":{'8080/tcp': ('127.0.0.2', 9001)} 
       },
    "manda-web-writeups" : {
       "ports":{'8080/tcp': ('127.0.0.2', 9002)} 
       },
    "manda-vpn" : {
       "volumes":["/home/ubuntu/vpn/users:/root/users:ro","/home/ubuntu/vpn/key:/root/key:ro"], 
       "privileged":True, 
       "ports":{'3000/tcp': ('0.0.0.0', 3000)}
       },
}

def keepuptodate(image, options, containers):
  logging.info(f'Pull {image}')
  image_obj = client.images.pull(f"reg.offensive.run/library/{image}:latest")
  if image not in containers.keys():
    logging.info(f'Starting {image}')
    client.containers.run(image_obj, name=image, detach=True, auto_remove=True, **options)
    logging.info(f'Started {image}')
    return
  
  container = client.containers.get(containers[image])
  if container.image.id != image_obj.id:
    logging.info(f'New image available for {image}, stopping existing container')
    container.rename(f"{image}-old")
    container.stop()
    logging.info(f'Old container has been stopped, starting a new container for {image}')
    client.containers.run(image_obj, name=image, detach=True, auto_remove=True, **options)
    logging.info(f'Updated container {image}')

def main():
  while True: 
    containers={}
    for container in client.containers.list():
        containers[container.name]=container.id
    for image, options in data.items():
        keepuptodate(image, options, containers) 
    time.sleep(30) 

if __name__ == "__main__":
    sys.exit(main())

