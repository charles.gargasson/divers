#!/bin/bash
cd -- "$( dirname -- "${BASH_SOURCE[0]}" )"
docker build . -t dockermanager:latest
docker stop dockermanager
docker rm dockermanager
docker run -d --restart="always" --name="dockermanager" -v "$(pwd)/main.py:/root/main.py:ro" -v "/var/run/docker.sock:/var/run/docker.sock" dockermanager:latest python3 /root/main.py

