import asyncio
from concurrent.futures import ThreadPoolExecutor
import time

"""
Async wait some IO blocking cmd using tread (here default non asyncio sleep)
"""

class Asynctest :
    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self.executor = ThreadPoolExecutor(max_workers=10) # Define Our Executor To Threads

    def Start(self):
        self.loop.run_until_complete(self.asyncmain())

    async def Asyncit(self, fn, *args, **kwargs) :
        future = self.loop.run_in_executor(self.executor, fn, *args, **kwargs) # 
        return asyncio.wrap_future(future)

    async def myasync(self):
        await self.Asyncit(time.sleep,1)
        await self.Asyncit(time.sleep,1)
        await self.Asyncit(time.sleep,1)
        await self.Asyncit(time.sleep,1)

    async def asyncmain(self):
        tasks = [ # Declare Concurrent Async Tasks
            self.myasync(),  
        ]
        await asyncio.gather(*tasks) # Wait for result

def main():
    s = Asynctest()
    s.Start()

if __name__ == '__main__' :
    main()