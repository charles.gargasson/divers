import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

depth=6
G = nx.balanced_tree(2, depth)
pos = graphviz_layout(G, prog='twopi', args='')
colors=['green']+int((len(G.nodes())-1)/2)*['red','blue']
nx.draw(G, pos, node_size=40 ,alpha=1, node_color=colors, with_labels=False)
plt.show()
