import matplotlib.pyplot as plt
x=[]
y=[]
num=50

for i in range(1,num+1):
    x.append(i)
    y.append(1/i)
 
# plotting the points
plt.plot(x, y, color='green', linestyle='solid', linewidth = 1,
         marker='o', markerfacecolor='blue', markersize=0)
 
# setting x and y axis range
plt.ylim(0,1)
plt.xlim(1,num)
 
plt.xlabel('x - axis')
plt.ylabel('y - axis')
plt.title('y=1/x')
plt.show()
