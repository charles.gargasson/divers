let iters=10
function run(){
    let cur=0
    let score=0
    for(let i=0;i<iters;i++){
        if (Math.random() < 0.5){
            cur++
            if (cur>score){score=cur;}
        } else {
            cur=0
        }
    }
    console.log(score)
    return score
}

while (run() < iters){}
