use rand;
use std::thread;

fn run(iterations:u64) {
    let mut serie_courante:u16 = 0;
    let mut serie_max:u16 = 0;
    let mut resultat_precedent:bool = false;
    println!("NEW THREAD");
    for _i in 1..iterations {
        let hasard:bool = rand::random();
        if resultat_precedent == hasard {
            serie_courante = serie_courante+1;
        } else if serie_courante > serie_max {
            serie_max = serie_courante;
            println!("NEW SCORE : {}",serie_max);
            serie_courante = 0;
        } else {
            serie_courante = 0;
            resultat_precedent = hasard;
        }
    }
    println!("MAX: {}",serie_max);
}

fn main() {
    const NTHREADS: u64 = 32;
    let iterations:u64 = 2u64.pow(36);
    println!("iterations : {}", iterations);

    let mut children = vec![];
    for i in 0..NTHREADS {
        children.push(thread::spawn(move || {
            run(iterations)
        }));
    }

    for child in children {
        let _ = child.join();
    }

    println!("iterations : {}",iterations*NTHREADS);
}


